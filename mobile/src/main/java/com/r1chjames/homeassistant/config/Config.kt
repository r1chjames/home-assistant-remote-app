package com.r1chjames.homeassistant.config

import android.content.Context
import com.r1chjames.homeassistant.R
import java.util.*

class Config {

    companion object {
        fun getConfigValue(context: Context, name: String): String {
            val resources = context.resources
            val rawResource = resources.openRawResource(R.raw.config)
            val properties = Properties()
            properties.load(rawResource)
            return properties.getProperty(name)

        }
    }

}