package com.r1chjames.homeassistant.service.model

data class SingleHomeAssistantState(
        val attributes: Attributes,
        val entity_id: String,
        val last_changed: String,
        val last_updated: String,
        val state: String
)