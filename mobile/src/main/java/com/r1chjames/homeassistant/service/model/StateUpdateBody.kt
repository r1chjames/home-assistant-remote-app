package com.r1chjames.homeassistant.service.model

data class StateUpdateBody(val entity_id: String)