package com.r1chjames.homeassistant.service.model

data class Attributes(
        val assumed_state: Boolean,
        val entity_id: List<String>,
        val friendly_name: String,
        val icon: String,
        val order: Int
)