package com.r1chjames.homeassistant.service.model

data class Context(
        val id: String,
        val user_id: Any
)