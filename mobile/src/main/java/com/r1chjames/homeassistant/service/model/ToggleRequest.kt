package com.r1chjames.homeassistant.service.model

data class ToggleRequest (
        val entity_id: String
)