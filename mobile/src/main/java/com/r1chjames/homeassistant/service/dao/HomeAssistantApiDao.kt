package com.r1chjames.homeassistant.service.dao

import com.r1chjames.homeassistant.service.model.HomeAssistantState
import com.r1chjames.homeassistant.service.model.ToggleRequest
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface HomeAssistantApiDao {

    @Headers("Accept: application/json")
    @POST("services/{service}/toggle")
    fun toggleService(
            @Path("service") service: String,
            @Header("Authorization") authorization: String,
            @Body body: ToggleRequest): Call<List<HomeAssistantState>>


    @GET("states")
    fun getAllStates(
            @Header("Authorization") authorization: String): Call<List<HomeAssistantState>>

    @GET("states/{group}")
    fun getSingleState(
            @Path("group") group: String,
            @Header("Authorization") authorization: String): Call<HomeAssistantState>

    companion object Factory {
        fun create(baseUrl: String): HomeAssistantApiDao {
            val retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            return retrofit.create(HomeAssistantApiDao::class.java)
        }
    }
}
