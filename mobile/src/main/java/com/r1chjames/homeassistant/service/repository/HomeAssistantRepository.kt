package com.r1chjames.homeassistant.service.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.util.Log
import com.r1chjames.homeassistant.config.Config
import com.r1chjames.homeassistant.service.dao.HomeAssistantApiDao
import com.r1chjames.homeassistant.service.model.HomeAssistantState
import com.r1chjames.homeassistant.service.model.ToggleRequest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Thread.sleep
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate

class HomeAssistantRepository constructor(private val context: Context, private val entity: String) {

    var retrofitService: HomeAssistantApiDao = HomeAssistantApiDao.create(Config.getConfigValue(context, "api_endpoint"))
    var hassState: MutableLiveData<HomeAssistantState> = MutableLiveData()
    val timer: Timer = Timer()
    init {
        timer.scheduleAtFixedRate(0, 5000) {
            refreshData()
        }
    }

    internal fun getData(): MutableLiveData<HomeAssistantState> { return hassState }

    internal fun toggleEntity(entity: String, service: String) {
        val body = ToggleRequest(entity)

        val call = retrofitService.toggleService(
                service,
                Config.getConfigValue(context, "api_auth"),
                body)

        Log.d("ENTITY_TOGGLE_REQUEST", call.request().toString() + " || " + call.request().headers() + " || " + body.toString() )
        call.enqueue(object: Callback<List<HomeAssistantState>> {
            override fun onResponse(call: Call<List<HomeAssistantState>>, response: retrofit2.Response<List<HomeAssistantState>>?) {
                if (response != null) {
                    Log.d("ENTITY_TOGGLE_RESPONSE", "$response.message() || $response.code()")
                }
            }

            override fun onFailure(call: Call<List<HomeAssistantState>>, t: Throwable) {
                Log.d("ERORR", "Failed to connect to Home Assistant")
            }
        })
        sleep(1000)
        refreshData()
    }

    private fun refreshData() {
        Log.d("BACKGROUNT_REFRESH", "Background refresh beginning")
        try{
            hassState.postValue(getEntityStateSync(entity))
        } catch (e: Exception) {
            Log.d("ERROR", "Socket timeout getting entity state")
        }
        Log.d("BACKGROUNT_REFRESH", "Background refresh finished")
    }

    internal fun getEntityStateAsync(entityId: String): LiveData<HomeAssistantState> {
        val call = retrofitService.getSingleState(entityId, Config.getConfigValue(context, "api_auth"))

        Log.d("ENTITY_STATE_ASYNC_REQUEST", call.request().toString() + "" + call.request().headers())

        val state: MutableLiveData<HomeAssistantState> = MutableLiveData()
        call.enqueue(object: Callback<HomeAssistantState> {
            override fun onResponse(call: Call<HomeAssistantState>, response: retrofit2.Response<HomeAssistantState>?) {
                if (response != null) {
                    state.value = response.body()
                    Log.d("ENTITY_STATE_ASYNC_RESPONSE", "$response.message() || $response.code()")
                }
            }

            override fun onFailure(call: Call<HomeAssistantState>, t: Throwable) {
                throw RuntimeException("Unable to connect to Home Assistant: $t")
            }
        })
        return state
    }

    internal fun getEntityStateSync(entityId: String): HomeAssistantState? {
        val call = retrofitService.getSingleState(entityId, Config.getConfigValue(context, "api_auth"))

        Log.d("ENTITY_STATE_SYNC_REQUEST", call.request().toString() + "" + call.request().headers())
        val response = makeCall(call)
        Log.d("ENTITY_STATE_SYNC_RESPONSE", "$response.message() || $response.code()")
        return response.body()
    }

    internal fun makeCall(call: Call<HomeAssistantState>): Response<HomeAssistantState> {
        return call.execute()
    }
}