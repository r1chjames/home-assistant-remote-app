//package com.r1chjames.homeassistant.viewmodel
//
//import android.app.Application
//import android.arch.lifecycle.AndroidViewModel
//import android.arch.lifecycle.LiveData
//import android.content.Context
//import com.r1chjames.homeassistant.config.Config
//import com.r1chjames.homeassistant.service.model.HomeAssistantState
//import com.r1chjames.homeassistant.service.repository.HomeAssistantRepository
//
//class ButtonViewModel(application: Application, context: Context) : AndroidViewModel(application) {
//
//    private val repo = HomeAssistantRepository(context)
//    var state: LiveData<HomeAssistantState> = repo.getHassStateForGroup(Config.getConfigValue(context, "hass_group_id"))
//
//}