package com.r1chjames.homeassistant.view.ui

import android.arch.lifecycle.Observer
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.StrictMode
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import com.r1chjames.homeassistant.R
import com.r1chjames.homeassistant.config.Config
import com.r1chjames.homeassistant.service.model.HomeAssistantState
import com.r1chjames.homeassistant.service.repository.HomeAssistantRepository
import java.net.SocketTimeoutException


class MainActivity : AppCompatActivity() {

    private lateinit var repo:HomeAssistantRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        repo = HomeAssistantRepository(this, Config.getConfigValue(this, "hass_group_id"))
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        title = Config.getConfigValue(this, "app_title")

        drawUi()
    }

    private fun drawUi() {
        val buttonLayout = findViewById<LinearLayout>(R.id.button_list)
        buttonLayout.removeAllViews()

        val buttonList: MutableList<Button> = mutableListOf()
        repo.getData().observe(this, Observer { group ->
            buttonList.add(createAllButton(Config.getConfigValue(this, "hass_group_id")))
            buttonList.add(createAutomationButton(Config.getConfigValue(this, "automation")))

            var buttonId = 2
            group!!.attributes.entity_id.filter { it.contains("light.") }.forEach{

                var state: HomeAssistantState? = null
                try {
                    state = repo.getEntityStateSync(it)
                } catch (e: SocketTimeoutException) {
                    Log.d("ERROR", "Socket timeout getting entity state")
                    Toast.makeText(this, "network failure :(", Toast.LENGTH_SHORT).show()
                    drawUi()
                }
                val button = Button(ContextThemeWrapper(this, R.style.LightToggleButton))
                button.id = buttonId
                button.hint = it

                button.text = state!!.attributes.friendly_name.toUpperCase()
                if (entityIsOn(state)) button.background.setColorFilter(button.context.resources.getColor(R.color.buttonOn), PorterDuff.Mode.MULTIPLY)
                else button.background.setColorFilter(button.context.resources.getColor(R.color.buttonOff), PorterDuff.Mode.MULTIPLY)

                buttonList.add(button)
                buttonId++
            }
            removeButtonsFromLayout(buttonList, buttonLayout)
            addButtonsToLayout(buttonList, buttonLayout)
        })
    }

    private fun entityIsOn(entityState: HomeAssistantState): Boolean {
        return entityState.state == "on"
    }

    private fun createAutomationButton(group: String): Button {
        val button = Button(ContextThemeWrapper(this, R.style.AutomationToggleButton))

        repo.getEntityStateAsync(group).observe(this, Observer { state ->
            button.id = 1
            button.hint = group

            button.text = state!!.attributes.friendly_name.toUpperCase()
            if (entityIsOn(state)) button.background.setColorFilter(button.context.resources.getColor(R.color.buttonOn), PorterDuff.Mode.MULTIPLY)
            else button.background.setColorFilter(button.context.resources.getColor(R.color.buttonOff), PorterDuff.Mode.MULTIPLY)

        })
        return button
    }

    private fun createAllButton(group: String): Button {
        val button = Button(ContextThemeWrapper(this, R.style.AllLightsToggleButton))

        repo.getEntityStateAsync(group).observe(this, Observer { state ->
            button.id = 0
            button.hint = group

            button.text = Config.getConfigValue(this, "all_lights_text").toUpperCase()
            if (entityIsOn(state!!)) button.background.setColorFilter(button.context.resources.getColor(R.color.buttonOn), PorterDuff.Mode.MULTIPLY)
            else button.background.setColorFilter(button.context.resources.getColor(R.color.buttonOff), PorterDuff.Mode.MULTIPLY)
        })
        return button
    }

    private fun addButtonToLayout(button: Button, layout: LinearLayout) {
        button.setOnClickListener {repo.toggleEntity(button.hint.toString(), getService(button.hint.toString()))}
        layout.addView(button)
    }

    private fun addButtonsToLayout(buttonList: MutableList<Button>, layout: LinearLayout) {
        buttonList.forEach { button -> addButtonToLayout(button, layout)
        }
    }

    private fun removeButtonsFromLayout(buttonList: MutableList<Button>, layout: LinearLayout) {
        buttonList.forEach { button -> removeButtonFromLayout(button, layout) }
    }

    private fun removeButtonFromLayout(button: Button, layout: LinearLayout) {
        val view = layout.findViewById<Button>(button.id)
        if (view != null) {
            layout.removeView(view)
            view.visibility = View.GONE
        }
    }

    private fun getService(entity: String): String {
        val service = entity.substring(0, entity.lastIndexOf('.'))
        return if (service != "group") {
            service
        } else {
            "light"
        }
    }


}
